//
//  NLFoundation.h
//  NLFoundation
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NLFoundation.
FOUNDATION_EXPORT double NLFoundationVersionNumber;

//! Project version string for NLFoundation.
FOUNDATION_EXPORT const unsigned char NLFoundationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NLFoundation/PublicHeader.h>


