//
//  Resolver+Extension.swift
//  NLFoundation
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Foundation
import Swinject

extension Resolver {
    public func forceResolve<T>(_: T.Type, name: String? = nil) -> T {
        return forceCast(self.resolve(T.self, name: name), T.self, justification: .injectionRequirement)
    }
}
