import NLApi
import NLFoundation

enum RestarauntSearchCommand {
    case startLoading
    case finishedLoading
    case present(RestarauntSearchViewState)
    case error(Error)
}

class RestarauntSearchKitchen: Kitchen {

    private enum Constants {
        static let keyword = "cruise"
        static let type = "restaurant"
    }

    typealias Command = RestarauntSearchCommand
    typealias ViewEvent = RestarauntSearchViewEvent

    weak var delegate: AnyKitchenDelegate<RestarauntSearchCommand>?

    private var restaurantService: RestaurantService
    private var viewStateFactory: RestarauntSearchViewStateFactory

    init(restarauntService: RestaurantService, viewStateFactory: RestarauntSearchViewStateFactory) {
        self.restaurantService = restarauntService
        self.viewStateFactory = viewStateFactory
    }

    func receive(event: RestarauntSearchViewEvent) {
        switch event {
        case .viewWillApper:
            handleViewWillAppear()
        }
    }

    private func handleViewWillAppear() {
        restaurantService.listOfRestaurant(with: Constants.keyword, type: Constants.type)
            .onComplete { [weak self] result in
                guard let strongSelf = self else {
                    return
                }

                switch result {
                case .success(let restaurants):
                    let viewState = strongSelf.viewStateFactory.make(from: restaurants)
                    strongSelf.delegate?.perform(.present(viewState))

                case .failure(let error):
                    strongSelf.delegate?.perform(.error(error))
                }
        }
    }
}
