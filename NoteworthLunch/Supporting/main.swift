//
//  main.swift
//  NoteworthLunch
//
//  Created by Alexandr Zhuchinskiy on 6/28/18.
//  Copyright © 2018 Alexandr Zhuchinskiy. All rights reserved.
//

import Foundation

import UIKit

private func applicationClassName() -> String? {
    return NSStringFromClass(Application.self)
}

UIApplicationMain(CommandLine.argc, UnsafeMutableRawPointer(CommandLine.unsafeArgv)
    .bindMemory(to: UnsafeMutablePointer<Int8>.self, capacity: Int(CommandLine.argc)), applicationClassName(), nil)
