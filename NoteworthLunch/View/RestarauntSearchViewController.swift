import NLFoundation

enum RestarauntSearchViewEvent {
    case viewWillApper
}

class RestarauntSearchViewController: UIViewController {

    private enum Constants {
        static let cellID = "RestarauntSearchResultCellID"
        static let estimatedCellHeight: CGFloat = 80.0
    }

    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = Constants.estimatedCellHeight
        }
    }

    @IBOutlet private var searchBar: UISearchBar!

    private var viewState: RestarauntSearchViewState? {
        didSet {
            tableView.reloadData()
        }
    }

    private var kitchen: AnyKitchen<RestarauntSearchViewEvent, RestarauntSearchCommand>!

    func inject(kitchen: AnyKitchen<RestarauntSearchViewEvent, RestarauntSearchCommand>) {
        self.kitchen = kitchen
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        kitchen.receive(event: .viewWillApper)
    }

    override func viewWillAppear(_ animated: Bool) {
        kitchen.receive(event: .viewWillApper)
    }

    private func showErrorBanner(with error: Error) {
        // TODO: - Implement show banner
    }
}

extension RestarauntSearchViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewState?.restaraunts.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellID, for: indexPath) as! RestarauntSearchResultCell
        cell.configure(with: forceCast(viewState!.restaraunts[indexPath.row], RestarauntSearchViewState.Restaraunt.self, justification: .cantBeNilAtThisPoint))
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension RestarauntSearchViewController: KitchenDelegate {
    func perform(_ command: RestarauntSearchCommand) {
        switch command {
        case .startLoading:
            break
        case .finishedLoading:
            break
        case .present(let viewState):
            self.viewState = viewState
        case .error(let error):
            showErrorBanner(with: error)
        }
    }
}
